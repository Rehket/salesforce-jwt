FROM python:3.8-alpine3.12
ARG CC_TEST_REPORTER_ID
RUN apk update && apk upgrade && apk add alpine-sdk python3-dev libffi-dev libressl-dev
RUN python -m pip install -U pip pip-tools
WORKDIR /app
COPY . /app
RUN pip-sync requirements.txt dev-requirements.txt
RUN python -m pip install -e .
RUN curl -L https://codeclimate.com/downloads/test-reporter/test-reporter-latest-linux-amd64 > ./cc-test-reporter && \
chmod +x ./cc-test-reporter && \
./cc-test-reporter before-build
RUN pytest --cov=sfjwt tests/
RUN coverage xml && \
./cc-test-reporter after-build
